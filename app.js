var app = require('express')();
var http = require('http').Server(app);
var fs = require('fs');
var io = require('socket.io')(http);

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});

io.on('connection', function(socket){
	fs.readFile("test2.log",myFlag, function(err, data){
		if (err) throw err;
		io.emit('newContent', data);
	});
});

var myFlag = {
	flags: 'r',
	encoding: 'utf8',
	autoClose: true
};

var prevfileSize = 0;
var fileName = "test2.log";




function myReadSyncFile(myobj,file, status, bytes){
	if(status == "truncate"){
		io.emit('log', bytes+" byte data truncated");
	}else{
		var readStream = fs.createReadStream(file,myobj);

		readStream
		.on('readable', function () {
			var chunk;
			while (null !== (chunk = readStream.read())) {
				io.emit('newContent', chunk);
				io.emit('log', bytes+" byte data added");
			}
		})
		.on('end', function () {
		});
	}
}


setInterval(function(){
	var stats = fs.statSync("test2.log");

	if(stats.size - prevfileSize > 0){
		var myopt = {
			flags: 'r',
			encoding: 'utf8',
			autoClose: true,
			start: prevfileSize,
			end: stats.size
		}
		myReadSyncFile(myopt,fileName,"appended",(stats.size - prevfileSize));

	}else if(stats.size - prevfileSize < 0){
		var myopt = {
			flags: 'r',
			encoding: 'utf8',
			autoClose: true,
			start: stats.size,
			end: prevfileSize
		}
		myReadSyncFile(myopt,fileName,"truncate",(prevfileSize - stats.size));

	}
	prevfileSize = stats.size;
}, 1000);
